# Online Library Application README

## Overview

Welcome to the Online Library Application! This application provides functionality for users to search for available books, make reservations, and review books. It's developed using Java 21 and Spring Boot 3.2.5, taking advantage of the latest language features and best practices.

## Setup and Execution

To set up and run the application, follow these steps:

1. **Clone the repository:** `git clone <repository_url>`
2. **Navigate to the project directory:** `cd <project_directory>`
3. **Ensure you have Java 21 and Maven installed on your system.**
4. **Build the project:** `mvn clean install`
5. **Run the application:** `mvn spring-boot:run`

## Running Unit Tests
To execute the unit tests, follow these steps:

1. **Locate the Test Files: Find the test files within the src/test/java directory in your project structure.**
2. **Execute Tests: Utilize the testing functionality provided by your IDE to execute the unit tests. In IntelliJ IDEA, right-click on a test file or directory and choose "Run `<test_name(s)>`". In Eclipse, right-click on a test file or directory and select "Run As" > "JUnit Test".**
3. **Review Test Outcomes: After running the tests, inspect the results displayed in the output console of your IDE. You'll see a summary of the tests executed and whether they passed or failed. Failed tests will provide insights into the causes of failure, aiding in debugging and resolution.**
4. **Address Failures: In case of any failed tests, analyze the code and rectify any identified issues. Rerun the tests to confirm that all tests pass successfully.**
5. **Update Version Control: Once all tests pass, commit your changes to your version control system (e.g., Git) along with the updated test outcomes.**



## Design Choices and Technologies Employed

The application is designed with a microservices architecture using Spring Boot, allowing for modular development and easy scalability. Key technologies and design choices include:

- **Spring Boot:** Leveraged for rapid application development and dependency management.
- **Spring Data JPA:** Used for easy interaction with the database and managing entity relationships.
- **Liquibase:** Employed for managing database schema evolution with controlled versioning.
- **Spring Boot Actuator:** Integrated for health monitoring and metrics exposure.
- **Caching:** Implemented caching to enhance performance, particularly for frequently accessed data like book listings.
- **Swagger:** Integrated for API documentation and testing.

## Observability and Monitoring

The application incorporates observability features to monitor its health, performance, and important events. To monitor the application:

- **Access the Actuator endpoints** (`/actuator/*`) to view health, metrics, and other monitoring data.
- **Configure logging** to record important events and track application behavior. Logs are stored in files for easy analysis.

## Database Schema Representation

The database schema is designed to accommodate the requirements of the application efficiently. Here's an overview of the schema:

- **Users:** Stores information about users, including username, email, and password.
- **Books:** Contains details of available books such as title, author, publication year, and ISBN.
- **Reservations:** Tracks reservations made by users for specific books, along with reservation date and status.
- **Reviews:** Stores user reviews for books, including rating, review text, and review date.

The schema is designed to maintain data integrity and optimize performance for operations like book searches, reservations, and reviews.

For a more detailed database schema representation, refer to the provided ERD or equivalent documentation.

## ERD Documentation 

                    +---------------+
                    |     users     |
                    +---------------+
                    | user_id (PK)  |
                    | username      |
                    | email         |
                    | password      |
                    +---------------+
                           |
                           |
                           |
                           | 1
                           |
                           |
                    +---------------+
                    |     books     |
                    +---------------+
                    | book_id (PK)  |
                    | title         |
                    | author        |
                    | publication_year |
                    | isbn          |
                    +---------------+
                           |
                           |
                           |
                           | 1
                           |
                           |
                    +---------------+
                    | reservations |
                    +---------------+
                    | reservation_id (PK) |
                    | user_id (FK)  |-----------------+
                    | book_id (FK)  |                 |
                    | reservation_date   |            1 |
                    | status        |                 |
                    +---------------+                 |
                           |                          |
                           |                          |
                           |                          |
                           | 1                        |
                           |                          |
                           |                          |
                    +---------------+                 |
                    |    reviews    |                 |
                    +---------------+                 |
                    | review_id (PK)|                 |
                    | user_id (FK)  |-----------------+
                    | book_id (FK)  |
                    | rating        |
                    | review_text   |
                    | review_date   |
                    +---------------+



## Conclusion

The Online Library Application offers a user-friendly interface for managing library resources efficiently. With its modern architecture and robust features, it provides a solid foundation for further enhancements and expansion. Enjoy exploring and using the application!
