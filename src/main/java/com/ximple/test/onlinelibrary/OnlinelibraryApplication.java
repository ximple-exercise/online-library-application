package com.ximple.test.onlinelibrary;

import org.hibernate.tool.schema.spi.CommandAcceptanceException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@SpringBootApplication
@EnableCaching
public class OnlinelibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlinelibraryApplication.class, args);
	}
	
    @Component
    public class SchemaCreationErrorHandler implements ApplicationListener<ApplicationReadyEvent> {

        @Override
        public void onApplicationEvent(ApplicationReadyEvent event) {
            try {
                // Lógica para inicializar tu aplicación
            } catch (CommandAcceptanceException e) {
                // Capturar la excepción específica de Hibernate y generar un mensaje personalizado
                System.out.println("Se ha producido un error durante la inicialización de la aplicación: La tabla ya existe en la base de datos.");
            } catch (Exception e) {
                // Manejar otras excepciones aquí
                System.out.println("Se ha producido un error durante la inicialización de la aplicación: " + e.getMessage());
            }
        }
    }


}
