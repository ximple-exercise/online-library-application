package com.ximple.test.onlinelibrary.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ximple.test.onlinelibrary.dto.BookDTO;
import com.ximple.test.onlinelibrary.entity.Book;
import com.ximple.test.onlinelibrary.service.BookService;

import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {
    
	private final BookService bookService;
	private static final Logger LOGGER = LoggerFactory.getLogger(BookController.class);
	
    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public ResponseEntity<List<Book>> getAllBooks() {
    	
    	LOGGER.info("Get All Books ...");
        List<Book> books = bookService.getAllBooks();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable("id") Long id) {
    	
    	LOGGER.info("Get Book By Id: {}", id);
        return bookService.getBookById(id)
                .map(book -> new ResponseEntity<>(book, HttpStatus.OK))
                .orElseGet(() -> {
                    LOGGER.warn("Book Dont Found By ID: {}", id);
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                });
    }

    @PostMapping
    public ResponseEntity<Book> createBook(@RequestBody BookDTO bookDTO) {
    	
    	LOGGER.info("Create Book: {}", bookDTO.getTitle());
        Book createdBook = bookService.createBook(bookDTO);
        return new ResponseEntity<>(createdBook, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable("id") Long id, @RequestBody BookDTO bookDTO) {
    	LOGGER.info("Update Book By Id: {}", id);
        Book updatedBook = bookService.updateBook(id, bookDTO);
        return new ResponseEntity<>(updatedBook, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBook(@PathVariable("id") Long id) {
    	
    	LOGGER.info("Delete Book By Id: {}", id);
        bookService.deleteBook(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
