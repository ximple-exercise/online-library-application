package com.ximple.test.onlinelibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {
	
    private Long bookId;
    private String title;
    private String author;
    private Integer publicationYear;
    private String isbn;


}
