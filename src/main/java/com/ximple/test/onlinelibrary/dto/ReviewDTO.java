package com.ximple.test.onlinelibrary.dto;

import java.time.LocalDateTime;

import com.ximple.test.onlinelibrary.entity.Book;
import com.ximple.test.onlinelibrary.entity.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDTO {
	
    private Long reviewId;
    private User userId;
    private Book bookId;
    private Integer rating;
    private String reviewText;
    private LocalDateTime reviewDate;
}
