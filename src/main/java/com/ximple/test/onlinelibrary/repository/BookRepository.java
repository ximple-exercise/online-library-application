package com.ximple.test.onlinelibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ximple.test.onlinelibrary.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
