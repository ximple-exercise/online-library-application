package com.ximple.test.onlinelibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ximple.test.onlinelibrary.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
