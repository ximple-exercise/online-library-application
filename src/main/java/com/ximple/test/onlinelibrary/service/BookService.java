package com.ximple.test.onlinelibrary.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.ximple.test.onlinelibrary.dto.BookDTO;
import com.ximple.test.onlinelibrary.entity.Book;
import com.ximple.test.onlinelibrary.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    
    @Cacheable("books")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBookById(Long id) {
        return bookRepository.findById(id);
    }

	public Book createBook(BookDTO bookDTO) {
        Book newBook = new Book();
        newBook.setTitle(bookDTO.getTitle());
        newBook.setAuthor(bookDTO.getAuthor());
        newBook.setPublicationYear(bookDTO.getPublicationYear());
        newBook.setIsbn(bookDTO.getIsbn());
        return bookRepository.save(newBook);
	}

	public Book updateBook(Long id, BookDTO bookDTO) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            Book existingBook = optionalBook.get();
            existingBook.setTitle(bookDTO.getTitle());
            existingBook.setAuthor(bookDTO.getAuthor());
            existingBook.setPublicationYear(bookDTO.getPublicationYear());
            existingBook.setIsbn(bookDTO.getIsbn());
            return bookRepository.save(existingBook);
        } else {
            throw new IllegalArgumentException("Book not found with id: " + id);
        }
	}

	public void deleteBook(Long id) {
		 bookRepository.deleteById(id);
		
	}

}
