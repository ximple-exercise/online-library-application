package com.ximple.test.onlinelibrary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ximple.test.onlinelibrary.dto.ReservationDTO;
import com.ximple.test.onlinelibrary.entity.Reservation;
import com.ximple.test.onlinelibrary.repository.ReservationRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    public Optional<Reservation> getReservationById(Long id) {
        return reservationRepository.findById(id);
    }
    
    public Reservation createReservation(ReservationDTO reservationDTO) {
        Reservation newReservation = new Reservation();
        newReservation.setUser(reservationDTO.getUserId());
        newReservation.setBook(reservationDTO.getBookId());
        newReservation.setReservationDate(reservationDTO.getReservationDate());
        newReservation.setStatus(reservationDTO.getStatus());
        return reservationRepository.save(newReservation);
    }

    public Reservation updateReservation(Long id, ReservationDTO reservationDTO) {
        Optional<Reservation> optionalReservation = reservationRepository.findById(id);
        if (optionalReservation.isPresent()) {
            Reservation existingReservation = optionalReservation.get();
            existingReservation.setUser(reservationDTO.getUserId());
            existingReservation.setBook(reservationDTO.getBookId());
            existingReservation.setReservationDate(reservationDTO.getReservationDate());
            existingReservation.setStatus(reservationDTO.getStatus());
            return reservationRepository.save(existingReservation);
        } else {
            throw new IllegalArgumentException("Reservation not found with id: " + id);
        }
    }
    
	public void deleteReservation(Long id) {
		reservationRepository.findById(id);
	}
 
}
