package com.ximple.test.onlinelibrary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ximple.test.onlinelibrary.dto.ReviewDTO;
import com.ximple.test.onlinelibrary.entity.Review;
import com.ximple.test.onlinelibrary.repository.ReviewRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ReviewService {
    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewService(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    public List<Review> getAllReviews() {
        return reviewRepository.findAll();
    }

    public Optional<Review> getReviewById(Long id) {
        return reviewRepository.findById(id);
    }

	public Review createReview(ReviewDTO reviewDTO) {
        Review newReview = new Review();
        newReview.setUser(reviewDTO.getUserId());
        newReview.setBook(reviewDTO.getBookId());
        newReview.setRating(reviewDTO.getRating());
        newReview.setReviewText(reviewDTO.getReviewText());
        newReview.setReviewDate(reviewDTO.getReviewDate());
        return reviewRepository.save(newReview);
	}

	public Review updateReview(Long id, ReviewDTO reviewDTO) {
		
        Optional<Review> optionalReview = reviewRepository.findById(id);
        if (optionalReview.isPresent()) {
            Review existingReview = optionalReview.get();
            existingReview.setUser(reviewDTO.getUserId());
            existingReview.setBook(reviewDTO.getBookId());
            existingReview.setRating(reviewDTO.getRating());
            existingReview.setReviewText(reviewDTO.getReviewText());
            existingReview.setReviewDate(reviewDTO.getReviewDate());
            return reviewRepository.save(existingReview);
        } else {
            throw new IllegalArgumentException("Review not found with id: " + id);
        }
    }

    
	public void deleteReview(Long id) {
		reviewRepository.findById(id);
	}

}
