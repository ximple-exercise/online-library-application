package com.ximple.test.onlinelibrary.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ximple.test.onlinelibrary.dto.BookDTO;
import com.ximple.test.onlinelibrary.entity.Book;
import com.ximple.test.onlinelibrary.service.BookService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class BookControllerTest {

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController bookController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllBooks_ReturnsListOfBooks() {
        // Arrange
        List<Book> expectedBooks = Arrays.asList(new Book(), new Book());
        when(bookService.getAllBooks()).thenReturn(expectedBooks);

        // Act
        ResponseEntity<List<Book>> response = bookController.getAllBooks();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBooks, response.getBody());
    }

    @Test
    void getBookById_BookExists_ReturnsBook() {
        // Arrange
        Long bookId = 1L;
        Book expectedBook = new Book();
        when(bookService.getBookById(bookId)).thenReturn(Optional.of(expectedBook));

        // Act
        ResponseEntity<Book> response = bookController.getBookById(bookId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedBook, response.getBody());
    }

    @Test
    void getBookById_BookDoesNotExist_ReturnsNotFound() {
        // Arrange
        Long bookId = 1L;
        when(bookService.getBookById(bookId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Book> response = bookController.getBookById(bookId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
    }

    @Test
    void createBook_ReturnsCreatedBook() {
        // Arrange
        BookDTO bookDTO = new BookDTO();
        Book createdBook = new Book();
        when(bookService.createBook(bookDTO)).thenReturn(createdBook);

        // Act
        ResponseEntity<Book> response = bookController.createBook(bookDTO);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdBook, response.getBody());
    }

    @Test
    void updateBook_BookExists_ReturnsUpdatedBook() {
        // Arrange
        Long bookId = 1L;
        BookDTO bookDTO = new BookDTO();
        Book updatedBook = new Book();
        when(bookService.updateBook(bookId, bookDTO)).thenReturn(updatedBook);

        // Act
        ResponseEntity<Book> response = bookController.updateBook(bookId, bookDTO);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedBook, response.getBody());
    }

    @Test
    void deleteBook_BookExists_ReturnsNoContent() {
        // Arrange
        Long bookId = 1L;

        // Act
        ResponseEntity<Void> response = bookController.deleteBook(bookId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(bookService, times(1)).deleteBook(bookId);
    }
}
