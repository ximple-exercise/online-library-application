package com.ximple.test.onlinelibrary.controller;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ximple.test.onlinelibrary.dto.ReservationDTO;
import com.ximple.test.onlinelibrary.entity.Reservation;
import com.ximple.test.onlinelibrary.service.ReservationService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ReservationControllerTest {

    @Mock
    private ReservationService reservationService;

    @InjectMocks
    private ReservationController reservationController;

    public ReservationControllerTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllReservations_ReturnsListOfReservations() {
        // Arrange
        List<Reservation> expectedReservations = Arrays.asList(new Reservation(), new Reservation());
        when(reservationService.getAllReservations()).thenReturn(expectedReservations);

        // Act
        ResponseEntity<List<Reservation>> response = reservationController.getAllReservations();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedReservations, response.getBody());
    }

    @Test
    void getReservationById_ReservationExists_ReturnsReservation() {
        // Arrange
        Long reservationId = 1L;
        Reservation expectedReservation = new Reservation();
        when(reservationService.getReservationById(reservationId)).thenReturn(Optional.of(expectedReservation));

        // Act
        ResponseEntity<Reservation> response = reservationController.getReservationById(reservationId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedReservation, response.getBody());
    }

    @Test
    void getReservationById_ReservationDoesNotExist_ReturnsNotFound() {
        // Arrange
        Long reservationId = 1L;
        when(reservationService.getReservationById(reservationId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Reservation> response = reservationController.getReservationById(reservationId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
    }

    @Test
    void createReservation_ReturnsCreatedReservation() {
        // Arrange
        ReservationDTO reservationDTO = new ReservationDTO();
        Reservation createdReservation = new Reservation();
        when(reservationService.createReservation(reservationDTO)).thenReturn(createdReservation);

        // Act
        ResponseEntity<Reservation> response = reservationController.createReservation(reservationDTO);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdReservation, response.getBody());
    }

    @Test
    void updateReservation_ReservationExists_ReturnsUpdatedReservation() {
        // Arrange
        Long reservationId = 1L;
        ReservationDTO reservationDTO = new ReservationDTO();
        Reservation updatedReservation = new Reservation();
        when(reservationService.updateReservation(reservationId, reservationDTO)).thenReturn(updatedReservation);

        // Act
        ResponseEntity<Reservation> response = reservationController.updateReservation(reservationId, reservationDTO);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedReservation, response.getBody());
    }

    @Test
    void deleteReservation_ReservationExists_ReturnsNoContent() {
        // Arrange
        Long reservationId = 1L;

        // Act
        ResponseEntity<Void> response = reservationController.deleteReservation(reservationId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(reservationService, times(1)).deleteReservation(reservationId);
    }
}
