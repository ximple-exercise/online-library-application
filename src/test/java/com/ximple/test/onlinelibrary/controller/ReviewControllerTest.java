package com.ximple.test.onlinelibrary.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ximple.test.onlinelibrary.dto.ReviewDTO;
import com.ximple.test.onlinelibrary.entity.Review;
import com.ximple.test.onlinelibrary.service.ReviewService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ReviewControllerTest {

    @Mock
    private ReviewService reviewService;

    @InjectMocks
    private ReviewController reviewController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllReviews_ReturnsListOfReviews() {
        // Arrange
        List<Review> expectedReviews = Arrays.asList(new Review(), new Review());
        when(reviewService.getAllReviews()).thenReturn(expectedReviews);

        // Act
        ResponseEntity<List<Review>> response = reviewController.getAllReviews();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedReviews, response.getBody());
    }

    @Test
    void getReviewById_ReviewExists_ReturnsReview() {
        // Arrange
        Long reviewId = 1L;
        Review expectedReview = new Review();
        when(reviewService.getReviewById(reviewId)).thenReturn(Optional.of(expectedReview));

        // Act
        ResponseEntity<Review> response = reviewController.getReviewById(reviewId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedReview, response.getBody());
    }

    @Test
    void getReviewById_ReviewDoesNotExist_ReturnsNotFound() {
        // Arrange
        Long reviewId = 1L;
        when(reviewService.getReviewById(reviewId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Review> response = reviewController.getReviewById(reviewId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
    }

    @Test
    void createReview_ReturnsCreatedReview() {
        // Arrange
        ReviewDTO reviewDTO = new ReviewDTO();
        Review createdReview = new Review();
        when(reviewService.createReview(reviewDTO)).thenReturn(createdReview);

        // Act
        ResponseEntity<Review> response = reviewController.createReview(reviewDTO);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdReview, response.getBody());
    }

    @Test
    void updateReview_ReviewExists_ReturnsUpdatedReview() {
        // Arrange
        Long reviewId = 1L;
        ReviewDTO reviewDTO = new ReviewDTO();
        Review updatedReview = new Review();
        when(reviewService.updateReview(reviewId, reviewDTO)).thenReturn(updatedReview);

        // Act
        ResponseEntity<Review> response = reviewController.updateReview(reviewId, reviewDTO);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedReview, response.getBody());
    }

    @Test
    void deleteReview_ReviewExists_ReturnsNoContent() {
        // Arrange
        Long reviewId = 1L;

        // Act
        ResponseEntity<Void> response = reviewController.deleteReview(reviewId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(reviewService, times(1)).deleteReview(reviewId);
    }
}
