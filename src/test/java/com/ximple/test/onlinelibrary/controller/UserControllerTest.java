package com.ximple.test.onlinelibrary.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ximple.test.onlinelibrary.dto.UserDTO;
import com.ximple.test.onlinelibrary.entity.User;
import com.ximple.test.onlinelibrary.service.UserService;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createUser_ShouldReturnCreatedUser() {
        // Arrange
        UserDTO userDTO = new UserDTO();
        User createdUser = new User();
        when(userService.createUser(any(UserDTO.class))).thenReturn(createdUser);

        // Act
        ResponseEntity<User> response = userController.createUser(userDTO);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(createdUser, response.getBody());
    }

    @Test
    void updateUser_ShouldReturnUpdatedUser() {
        // Arrange
        Long userId = 1L;
        UserDTO userDTO = new UserDTO();
        User updatedUser = new User();
        when(userService.updateUser(eq(userId), any(UserDTO.class))).thenReturn(updatedUser);

        // Act
        ResponseEntity<User> response = userController.updateUser(userId, userDTO);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedUser, response.getBody());
    }

    @Test
    void deleteUser_ShouldReturnNoContent() {
        // Arrange
        Long userId = 1L;

        // Act
        ResponseEntity<Void> response = userController.deleteUser(userId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(userService, times(1)).deleteUser(userId);
    }
}
